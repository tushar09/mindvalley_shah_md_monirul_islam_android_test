package tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.MainActivity;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.R;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.databinding.RowBinding;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind.MindOPT;

/**
 * Created by Tushar on 10/29/2016.
 */

public class MindListViewAdapter extends BaseAdapter {

    ArrayList id, like, profileImage, html, picUrlRegular, picUrlFull, title, name, download, liked, picUrlRaw, picUrlSmall, picUrlThumb;
    Context context;
    private int lastPosition = -1;

    public MindListViewAdapter(MainActivity mainActivity, ArrayList name, ArrayList like, ArrayList profileImage, ArrayList picUrlRegular, ArrayList picUrlFull, ArrayList title, ArrayList html, ArrayList download, ArrayList liked, ArrayList picUrlRaw, ArrayList picUrlSmall, ArrayList picUrlThumb) {
        context = mainActivity;
        this.title = title;
        this.name = name;
        this.like = like;
        this.picUrlFull = picUrlFull;
        this.picUrlRegular = picUrlRegular;
        this.profileImage = profileImage;
        this.html = html;
        this.download = download;
        this.liked = liked;
        this.picUrlRaw = picUrlRaw;
        this.picUrlSmall = picUrlSmall;
        this.picUrlThumb = picUrlThumb;
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = holder.binding.getRoot();
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        holder.binding.textView.setText("" + name.get(position));
        holder.binding.textView2.setText("" + title.get(position));
        holder.binding.likeAmount.setText("" + like.get(position));

        if(liked.get(position).equals("0")){
            holder.binding.like.setBackgroundResource(R.drawable.ic_favorite_white_24dp);
        }else if(liked.get(position).equals("1")){
            holder.binding.like.setBackgroundResource(R.drawable.ic_favorite_red_24dp);
        }

        MindOPT.loadImage(context, holder.binding.profileImage, "" + profileImage.get(position));
        MindOPT.loadImage(context, holder.binding.image, "" + picUrlRegular.get(position));

        //Love button click operation.
        holder.binding.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(liked.get(position).equals("0")){
                    Animation expandIn = AnimationUtils.loadAnimation(context, R.anim.expand_in);
                    liked.set(position, "1");
                    like.set(position, Integer.parseInt("" + like.get(position)) + 1);
                    holder.binding.like.setBackgroundResource(R.drawable.ic_favorite_red_24dp);
                    holder.binding.likeAmount.setText("" + like.get(position));
                    Snackbar.make(v, "You just liked this post. Its for test", Snackbar.LENGTH_LONG).show();
                    holder.binding.likeAmount.startAnimation(expandIn);
                    holder.binding.like.startAnimation(expandIn);
                }else if(liked.get(position).equals("1")){
                    Animation expandIn = AnimationUtils.loadAnimation(context, R.anim.expand_in);
                    liked.set(position, "0");
                    like.set(position, Integer.parseInt("" + like.get(position)) - 1);
                    holder.binding.like.setBackgroundResource(R.drawable.ic_favorite_white_24dp);
                    holder.binding.likeAmount.setText("" + like.get(position));
                    Snackbar.make(v, "You just disliked this post. Its for test", Snackbar.LENGTH_LONG).show();
                    holder.binding.likeAmount.startAnimation(expandIn);
                    holder.binding.like.startAnimation(expandIn);
                }
            }
        });

        holder.binding.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);
                final MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.download, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId() == R.id.raw){
                            MindOPT.download(context, "" + picUrlRaw.get(position));
                        }else if(item.getItemId() == R.id.full){
                            MindOPT.download(context, "" + picUrlFull.get(position));
                        }
                        else if(item.getItemId() == R.id.regular){
                            MindOPT.download(context, "" + picUrlRegular.get(position));
                        }
                        else if(item.getItemId() == R.id.small){
                            MindOPT.download(context, "" + picUrlSmall.get(position));
                        }
                        else if(item.getItemId() == R.id.thumb){
                            MindOPT.download(context, "" + picUrlThumb.get(position));
                        }
                        return false;
                    }
                });

            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "" + html.get(position);
                if (!url.startsWith("http://") && !url.startsWith("https://")){
                    url = "http://" + url;
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);
            }
        });

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;
        return convertView;
    }

    private class Holder{
        RowBinding binding;
        Holder(){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row, null, true);
        }
    }

}
