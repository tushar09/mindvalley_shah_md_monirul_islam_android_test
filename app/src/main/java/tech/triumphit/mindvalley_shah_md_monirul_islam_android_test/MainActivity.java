package tech.triumphit.mindvalley_shah_md_monirul_islam_android_test;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.adapter.MindListViewAdapter;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.databinding.ActivityBinding;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind.MindDataReader;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind.MindOPT;
import tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind.ResponseListener;

public class MainActivity extends AppCompatActivity implements ResponseListener.OnResponseListener, SwipeRefreshLayout.OnRefreshListener {

    ActivityBinding binding;
    Animation animDown;
    private Animation animDownPostButon, animDownPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity);
        setSupportActionBar(binding.toolbar);

        getSupportActionBar().setTitle("Home");


        binding.sw.setOnRefreshListener(this);


        binding.sw.post(new Runnable() {
                            @Override
                            public void run() {
                                binding.sw.setRefreshing(true);
                                getFeed();
                            }
                        }
        );


        binding.sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        //this is just to show the animation.
        binding.postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                binding.loadingView.smoothToShow();
                binding.loadingView.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        binding.loadingView.smoothToHide();
                                                        Snackbar.make(v, "API is not implemented yet.", Snackbar.LENGTH_LONG).show();
                                                    }
                                                }, 3000
                );
            }
        });

        //Image picker
        binding.postPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Photo")
                        .setMessage("Take Photo from camera or folder")
                        .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                                startActivityForResult(intent, 420);
                            }
                        })
                        .setNegativeButton("Folder", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<Image> images = new ArrayList<Image>();
                                ImagePicker.create(MainActivity.this)
                                        .folderMode(true) // folder mode (false by default)
                                        .folderTitle("Folder") // folder selection title
                                        .imageTitle("Tap to select") // image selection title
                                        .single() // single mode
                                        .multi() // multi mode (default mode)
                                        .limit(1) // max images can be selected (99 by default)
                                        .showCamera(false) // show camera or not (true by default)
                                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                                        .origin(images) // original selected images, used in multi mode
                                        .start(500); // start image picker activity with request code
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 500 && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            MindOPT.loadImageWithAnimation(MainActivity.this, binding.imageView11, images.get(0).getPath(), R.anim.slide_in_right);
        }
        if (requestCode == 420) {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            MindOPT.loadImageWithAnimation(MainActivity.this, binding.imageView11, file.getPath(), R.anim.slide_in_right);
        }
    }


    /**
     * This method is used to avoid the thumbnail of the bitmap and to get th e full size image.
     * @param path
     * Image file path
     * @param reqWidth
     * Width of the new bitmap
     * @param reqHeight
     * Height of the new bitmap
     * @return
     * Return Bitmap
     */
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }


    boolean anim = true;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.postHomeMenu) {
            if (anim) {
                animatePosterVisible();
                anim = false;
                Log.e("should animante ", "shw");
            } else {
                animatePosterInvisible();
                Log.e("should animante ", "dnt");
                anim = true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void OnResponse(String response, int type) {
        if (type == MindDataReader.JSON) {
            //its json response
            extractDataFromJSON(response);
        } else if (type == MindDataReader.XML) {
            //its xml response
        }
    }


    /**
     * this method is used to extract data from JSON response
     *
     * @param response JSON response in string.
     */
    private void extractDataFromJSON(String response) {
        final ArrayList id, like, profileImage, html, picUrlRegular, picUrlFull, title, name, download, liked, picUrlRaw, picUrlSmall, picUrlThumb;

        id = new ArrayList();
        name = new ArrayList();
        like = new ArrayList();
        profileImage = new ArrayList();
        html = new ArrayList();
        picUrlRegular = new ArrayList();
        picUrlFull = new ArrayList();
        title = new ArrayList();
        download = new ArrayList();
        liked = new ArrayList();
        picUrlRaw = new ArrayList();
        picUrlSmall = new ArrayList();
        picUrlThumb = new ArrayList();


        try {
            JSONArray jr = new JSONArray(response);
            Log.e("error", jr.length() + "");
            for (int t = 0; t < jr.length(); t++) {
                JSONObject jsonObject = jr.getJSONObject(t);
                id.add("" + jsonObject.getString("id"));
                like.add("" + jsonObject.getString("likes"));

                JSONObject user = jsonObject.getJSONObject("user");
                name.add(user.getString("name"));
                JSONObject profile = user.getJSONObject("profile_image");
                profileImage.add(profile.getString("large"));

                JSONObject urls = jsonObject.getJSONObject("urls");
                picUrlRegular.add(urls.getString("regular"));
                picUrlFull.add(urls.getString("full"));
                picUrlRaw.add(urls.getString("raw"));
                picUrlSmall.add(urls.getString("small"));
                picUrlThumb.add(urls.getString("thumb"));

                JSONArray categories = jsonObject.getJSONArray("categories");
                title.add(categories.getJSONObject(0).getString("title"));

                JSONObject links = jsonObject.getJSONObject("links");
                html.add(links.getString("html"));
                download.add(links.getString("download"));

                //Assumed for initial.
                liked.add("0");

                Log.e("error", links.getString("download"));
            }

            binding.lv.setAdapter(new MindListViewAdapter(MainActivity.this, name, like, profileImage, picUrlRegular, picUrlFull, title, html, download, liked, picUrlRaw, picUrlSmall, picUrlThumb));
            binding.sw.setRefreshing(false);

        } catch (JSONException e) {
            Log.e("error", e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void OnError(String error) {

    }

    @Override
    public void onRefresh() {
        getFeed();
    }

    public void getFeed() {
        MindDataReader.getResponse(this, "http://pastebin.com/raw/wgkJgazE");
    }


    /**
     * Animate the view to make it visible which is slide down when send button is clicked located on actionbar
     */
    private void animatePosterVisible() {
        binding.ril.setVisibility(View.VISIBLE);
        animDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        animDownPost = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down_child);
        animDownPostButon = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down_child);
        animDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

                binding.post.startAnimation(animDownPost);
                binding.postButton.startAnimation(animDownPostButon);
                binding.postPic.startAnimation(animDownPostButon);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.ril.startAnimation(animDown);
    }

    /**
     * Animate the view to make it invisible which is slide up when post button is clicked located under the edittext.
     */
    private void animatePosterInvisible() {
        animDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        animDownPost = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up_child);
        animDownPostButon = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up_child);
        animDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                binding.post.startAnimation(animDownPost);
                binding.postButton.startAnimation(animDownPostButon);
                binding.postPic.startAnimation(animDownPostButon);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //binding.ril.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.ril.startAnimation(animDown);

    }
}
