package tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tushar on 10/28/2016.
 */

public class MindDataReader {

    private static final String TIMEOUT = "Timeout";
    private static final String NO_CONNECTION = "No Connections";
    private static final String AUTH_FAILURE = "Auth Failed";
    private static final String SERVER_ERROR = "Server Error";
    private static final String NETWORK_ERROR = "Network Error";
    private static final String PARSE_ERROR = "Parse Error";

    public static int XML = 0;
    public static int JSON = 1;

    /**
     * Request for api call and response
     * @param context
     * Activity's context
     * @param url
     * API url
     */
    public static void getResponse(final Context context, String url) {
        final ResponseListenerExecutor responseListenerExecutor = new ResponseListenerExecutor();
        responseListenerExecutor.prepare((ResponseListener.OnResponseListener) context);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.startsWith("{") || response.startsWith("[")){
                            responseListenerExecutor.shootResponse(response, JSON);
                        }
                        if(response.startsWith("<")){
                            responseListenerExecutor.shootResponse(response, XML);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                            responseListenerExecutor.shootError("Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                            responseListenerExecutor.shootError(TIMEOUT);
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                            responseListenerExecutor.shootError(NO_CONNECTION);
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                            responseListenerExecutor.shootError(AUTH_FAILURE);
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                            responseListenerExecutor.shootError(SERVER_ERROR);
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                            responseListenerExecutor.shootError(NETWORK_ERROR);
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                            responseListenerExecutor.shootError(PARSE_ERROR);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(sr);
    }

    /**
     * Request for api call and response with post parameters.
     * @param context
     * Activity's context
     * @param url
     * API url
     * @param map
     * Map of post parameters
     */
    public static void getResponseWithPost(final Context context, String url, final Map map) {
        final ResponseListenerExecutor responseListenerExecutor = new ResponseListenerExecutor();
        responseListenerExecutor.prepare((ResponseListener.OnResponseListener) context);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseListenerExecutor.shootResponse(response, 0);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                            responseListenerExecutor.shootError("Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                            responseListenerExecutor.shootError(TIMEOUT);
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                            responseListenerExecutor.shootError(NO_CONNECTION);
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                            responseListenerExecutor.shootError(AUTH_FAILURE);
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                            responseListenerExecutor.shootError(SERVER_ERROR);
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                            responseListenerExecutor.shootError(NETWORK_ERROR);
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                            responseListenerExecutor.shootError(PARSE_ERROR);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return map;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(sr);
    }

}
