package tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind;

/**
 * Created by Tushar on 10/28/2016.
 */

public class ResponseListener {


    public interface OnResponseListener {
        /**
         * Network Error Listener
         *
         * @param error error is the string which contains the info about the failure of api call due to the bad network.
         */
        void OnError(String error);

        /**
         * Response Listener of api call. type can be compared with {@link MindDataReader#XML} or {@link MindDataReader#JSON}
         *
         * @param response
         * response is the result off the api call.
         * @param type
         * type is response category. JOSN or XML.
         */
        void OnResponse(String response, int type);

    }
}

