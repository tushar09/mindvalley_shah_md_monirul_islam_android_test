package tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind;

/**
 * Created by Tushar on 10/28/2016.
 */

public class ResponseListenerExecutor {
    ResponseListener.OnResponseListener responseListener;

    /**
     * Initialize the listener Interface
     * @param responseListener
     * OnResponseListener Object
     */
    public void prepare(ResponseListener.OnResponseListener responseListener){
        this.responseListener = responseListener;
    }

    /**
     * Send the api response in string
     * @param response
     * Api response
     * @param type
     * Response category
     */
    public void shootResponse(String response, int type){
        responseListener.OnResponse(response, type);
    }

    /**
     * Send Error report.
     * @param error
     * Error details
     */
    public void shootError(String error){
        responseListener.OnError(error);
    }

}
