package tech.triumphit.mindvalley_shah_md_monirul_islam_android_test.mind;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by Tushar on 10/28/2016.
 */
public class MindOPT {

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param url
     * Image url String
     */
    public static void loadImage(Context context, ImageView v, String url){
        Glide.with(context).load(url).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param drawable
     * Drawable int
     */
    public static void loadImage(Context context, ImageView v, int drawable){
        Glide.with(context).load(drawable).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param b
     * Bitmap
     */
    public static void loadImage(Context context, ImageView v, Bitmap b){
        Bitmap bitmap = b;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        Glide.with(context).load(byteArrayOutputStream.toByteArray()).asBitmap().into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param url
     * Image url String
     * @param anim
     * Drawable Animation
     */
    public static void loadImageWithAnimation(Context context, ImageView v, String url, int anim){
        Glide.with(context).load(url).animate(anim).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param url
     * Image url String
     * @param anim
     * Animator Animation
     */
    public static void loadImageWithAnimation(Context context, ImageView v, String url, Animation anim){
        Glide.with(context).load(url).animate(anim).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param drawable
     * drawable int
     * @param anim
     * Drawable Animation
     */
    public static void loadImageWithAnimation(Context context, ImageView v, int drawable, int anim){
        Glide.with(context).load(drawable).animate(anim).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param drawable
     * drawable int
     * @param anim
     * Animator Animation
     */
    public static void loadImageWithAnimation(Context context, ImageView v, int drawable, Animation anim){
        Glide.with(context).load(drawable).animate(anim).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param b
     * Bitmap
     * @param anim
     * Drawable Animation
     */
    public static void loadImageWithAnimation(Context context, ImageView v, ByteArrayOutputStream b, int anim){
        Glide.with(context).load(b.toByteArray()).asBitmap().animate(anim).into(v);
    }

    /**
     * Use this method to load image in desired ImageView.
     * @param context
     * Context of an Activity
     * @param v
     * ImageView
     * @param b
     * Bitmap
     * @param anim
     * Animator Animation
     */
    public static void loadImageWithAnimation(Context context, ImageView v, ByteArrayOutputStream b, Animation anim){
        Glide.with(context).load(b.toByteArray()).asBitmap().animate(anim).into(v);
    }

    /**
     * Use this method to download image
     * @param context
     * Context of an Activity
     * @param url
     * Url of image
     */
    public static void download(final Context context, final String url){
        //Glide.with(context).load(url).downloadOnly(2000, 2000);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Bitmap resource = Glide.with(context).load(url).asBitmap().into(1000, 1000).get();
                    String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/MindValley";
                    File dir = new File(file_path);
                    if(!dir.exists())
                        dir.mkdirs();
                    File file = new File(dir, "Mind_" + new Date().getTime() + ".png");
                    FileOutputStream fOut = new FileOutputStream(file);

                    resource.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    Log.e("download path", file.getAbsolutePath());
                    Log.e("download", "downloaded");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e("InterruptedException", e.toString());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    Log.e("ExecutionException", e.toString());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e("FileNotFoundException", e.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("IOException", e.toString());
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void dummy) {
                Toast.makeText(context, "Download Completed", Toast.LENGTH_LONG).show();
            }
        }.execute();

    }



}
