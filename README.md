# README #

# Mindvalley Test For SHAH MD MONIRUL ISLAM #

# *HOW TO USE* #

### API Response Part ###

* Clone this project.
* Implement `ResponseListener.OnResponseListener` for API listener. 
* Then call `MindDataReader.getResponse(this, "Your_API_URL");` in your activity or fragment.
* As result you will get api response in here:
```
#!java

@Override
public void OnResponse(String response, int type) {
    if (type == MindDataReader.JSON) {
        //its json response
    } else if (type == MindDataReader.XML) {
        //its xml response
    }
}
```
* And also the error will be sent in here:

```
#!java

@Override
public void OnError(String error) {

}
```


###Image Loading Part ###

* You jsut need to pass your context, view and image url to load image in you view from any where.

```
#!java

MindOPT.loadImage(your_context, your_imageview, "Your_Image_URL or drawable");
MindOPT.loadImageWithAnimation(your_context, your_imageview, "Your_Image_URL or drawable", R.anim.your_anim_xml or Animation your_animation);
```
* Or you can downblaod the image

```
#!java

MindOPT.download(your_context , "your_image_url");
```



### Contact ###

* Feel free to contact at wtushar09@gmail.com